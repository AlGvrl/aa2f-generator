void calculateParikhVector (char factor[], long factorLength, long vector[]);

char isAlmostAbelianSquareFree (char factor[], long factorLength);

void generateAA2F (char word[], long wordLength, void (*wordCallback)(char[], long, char));

void generateAA2FR (char word[], long wordLength, void (*wordCallback)(char[], long, char));

void printLengthOnly (char word[], long wordLength, char lastCall);

void printOnlyWordsOfIncreasingLength (char word[], long wordLength, char lastCall);

void printWord (char word[], long wordLength, char lastCall);

void printWordAndWait (char word[], long wordLength, char lastCall);

void printWordsLongerThanGivenLength (char word[], long wordLength, char lastCall);

void countWordsOfGivenLength (char word[], long wordLength, char lastCall);