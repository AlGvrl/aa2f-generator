# AA2F Generator

The original single-threaded version of a program looking for almost-abelian-square-free (that is, there are no substrings that are anagrams of each other side by side) strings.

Later was replaced by parallelized versions written in [JS](../../../aa2f-node) and [Rust](../../../aa2f-rs) respectively.

Related: https://bitbucket.org/kumanon/aa2f-with-c