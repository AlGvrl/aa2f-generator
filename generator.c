#include <stdio.h>
#ifdef __unix__
# include <unistd.h>
#elif defined _WIN32
# include <windows.h>
#define sleep(x) Sleep(1000 * x)
#endif
#include "generator.h"
#include "generator.conf.h"

int main() {
    char word[MAX_WORD_LENGTH+1] = STARTING_WORD;
    generateAA2FR(word, STARTING_WORD_LENGTH, &printWord);
    return 0;
}

void calculateParikhVector (char factor[], long factorLength, long vector[]) {
    long i = 0;
    for (i = 0; i < CARDINALITY_OF_ALPHABET; i++) {
        vector[i] = 0;
    }
    for (i = 0; i < factorLength; i++) {
        vector[factor[i]-FIRST_CHARACTER_OF_ALPHABET]++;
    }
}

//only the factors that include the last character are checked
//because it is supposed, that all the other factors were checked during previous iterations
//returns 1 if factor passes the test, 0 otherwise
char isAlmostAbelianSquareFree (char factor[], long factorLength) {
    static long parikh1[CARDINALITY_OF_ALPHABET];
    static long parikh2[CARDINALITY_OF_ALPHABET];
    long i = 0;
    if (factorLength < 4) { //we allow all non-A2F pairs of characters; singles and triples are A2F by definition
        return 1;
    }
    for (i = 2; i <= factorLength / 2; i++) {
        calculateParikhVector(factor+factorLength-(i * sizeof(char)), i, parikh1);
        calculateParikhVector(factor+factorLength-(2 * i * sizeof(char)), i, parikh2);
        if (!memcmp(parikh1, parikh2, CARDINALITY_OF_ALPHABET * sizeof(long))) {
            return 0;
        }
    }
    return 1;
}

void generateAA2F (char word[], long wordLength, void (*wordCallback)(char[], long, char)) {
    char i = 0;
    word[wordLength] = '\0';
    word[wordLength+1] = '\0';
    (*wordCallback)(word, wordLength, 0);
    if (wordLength >= MAX_WORD_LENGTH) {
        return;
    }
    for (i = 0; i < CARDINALITY_OF_ALPHABET; i++) {
        word[wordLength] = FIRST_CHARACTER_OF_ALPHABET + i;
        if (isAlmostAbelianSquareFree(word, wordLength + 1)) {
            generateAA2F(word, wordLength + 1, wordCallback);
        }
    }
}

void generateAA2FR (char word[], long wordLength, void (*wordCallback)(char[], long, char)) {
    char i = 0;
    word[wordLength] = '\0';
    word[wordLength+1] = '\0';
    if (((wordLength < 2) || word[wordLength - 1] != word[wordLength - 2])
        || ((wordLength >= 3) && (word[wordLength - 1] == word[wordLength - 2]) && (word[wordLength - 1] == word[wordLength - 3]))) {
        (*wordCallback)(word, wordLength, 0);
    }
    if (wordLength >= MAX_WORD_LENGTH) {
        return;
    }
    if (((wordLength >= 2) && (word[wordLength - 1] == word[wordLength - 2]))
        && !((wordLength >= 3) && (word[wordLength - 1] == word[wordLength - 2]) && (word[wordLength - 1] == word[wordLength - 3]))) {
        word[wordLength] = word[wordLength - 1];
        if (isAlmostAbelianSquareFree(word, wordLength + 1)) {
            generateAA2FR(word, wordLength + 1, wordCallback);
        }
    } else {
        for (i = 0; i < CARDINALITY_OF_ALPHABET; i++) {
            word[wordLength] = FIRST_CHARACTER_OF_ALPHABET + i;
            if (isAlmostAbelianSquareFree(word, wordLength + 1)) {
                generateAA2FR(word, wordLength + 1, wordCallback);
            }
        }
    }
}

void printLengthOnly (char word[], long wordLength, char lastCall) {
    printf ("%li\n", wordLength);
}

void printOnlyWordsOfIncreasingLength (char word[], long wordLength, char lastCall) {
    static long maxLengthAchieved;
    if (wordLength > maxLengthAchieved) {
        maxLengthAchieved = wordLength;
        printf ("%li %s\n", wordLength, word);
    }
}

void printWord (char word[], long wordLength, char lastCall) {
    printf ("%li %s\n", wordLength, word);
}

void printWordAndWait (char word[], long wordLength, char lastCall) {
    printf ("%li %s\n", wordLength, word);
    if (DELAY_AFTER_EACH_WORD > 0) {
        sleep(DELAY_AFTER_EACH_WORD);
    }
}

void printWordsLongerThanGivenLength (char word[], long wordLength, char lastCall) {
    if (wordLength > MIN_PRINTED_WORD_LENGTH) {
        printf ("%li %s\n", wordLength, word);
    }
}

void countWordsOfGivenLength (char word[], long wordLength, char lastCall) {
    static long counter;
    if ((wordLength > 0) && (wordLength % LENGTH_TO_COUNT == 0)) {
        counter++;
    }
    if (lastCall) {
        printf ("%li\n", counter);
    }
}